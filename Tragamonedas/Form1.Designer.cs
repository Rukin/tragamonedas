﻿namespace Tragamonedas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn50 = new System.Windows.Forms.Button();
            this.btn1euro = new System.Windows.Forms.Button();
            this.btn2euro = new System.Windows.Forms.Button();
            this.label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtjugado = new System.Windows.Forms.TextBox();
            this.txtcredito = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnjugar = new System.Windows.Forms.Button();
            this.btnCobrar = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.SuspendLayout();
            // 
            // btn50
            // 
            this.btn50.Location = new System.Drawing.Point(63, 7);
            this.btn50.Name = "btn50";
            this.btn50.Size = new System.Drawing.Size(75, 23);
            this.btn50.TabIndex = 0;
            this.btn50.Text = "50 cents";
            this.btn50.UseVisualStyleBackColor = true;
            this.btn50.Click += new System.EventHandler(this.btn50_Click);
            // 
            // btn1euro
            // 
            this.btn1euro.Location = new System.Drawing.Point(144, 7);
            this.btn1euro.Name = "btn1euro";
            this.btn1euro.Size = new System.Drawing.Size(75, 23);
            this.btn1euro.TabIndex = 1;
            this.btn1euro.Text = "1 €";
            this.btn1euro.UseVisualStyleBackColor = true;
            this.btn1euro.Click += new System.EventHandler(this.btn1euro_Click);
            // 
            // btn2euro
            // 
            this.btn2euro.Location = new System.Drawing.Point(225, 7);
            this.btn2euro.Name = "btn2euro";
            this.btn2euro.Size = new System.Drawing.Size(75, 23);
            this.btn2euro.TabIndex = 2;
            this.btn2euro.Text = "2 €";
            this.btn2euro.UseVisualStyleBackColor = true;
            this.btn2euro.Click += new System.EventHandler(this.btn2euro_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(60, 45);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(43, 13);
            this.label.TabIndex = 3;
            this.label.Text = "Crédito:";
            this.label.Click += new System.EventHandler(this.label_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(60, 79);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Jugado:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtjugado
            // 
            this.txtjugado.Location = new System.Drawing.Point(144, 79);
            this.txtjugado.Name = "txtjugado";
            this.txtjugado.ReadOnly = true;
            this.txtjugado.Size = new System.Drawing.Size(156, 20);
            this.txtjugado.TabIndex = 5;
            this.txtjugado.Text = "0";
            this.txtjugado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtjugado.TextChanged += new System.EventHandler(this.txtjugado_TextChanged);
            // 
            // txtcredito
            // 
            this.txtcredito.Location = new System.Drawing.Point(144, 45);
            this.txtcredito.Name = "txtcredito";
            this.txtcredito.ReadOnly = true;
            this.txtcredito.Size = new System.Drawing.Size(156, 20);
            this.txtcredito.TabIndex = 6;
            this.txtcredito.Text = "0";
            this.txtcredito.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtcredito.TextChanged += new System.EventHandler(this.txtcredito_TextChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Tragamonedas.Properties.Resources.limon;
            this.pictureBox1.Location = new System.Drawing.Point(26, 115);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(105, 101);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // btnjugar
            // 
            this.btnjugar.Location = new System.Drawing.Point(98, 491);
            this.btnjugar.Name = "btnjugar";
            this.btnjugar.Size = new System.Drawing.Size(89, 28);
            this.btnjugar.TabIndex = 16;
            this.btnjugar.Text = "JUGAR";
            this.btnjugar.UseVisualStyleBackColor = true;
            this.btnjugar.Click += new System.EventHandler(this.btnjugar_Click);
            // 
            // btnCobrar
            // 
            this.btnCobrar.Location = new System.Drawing.Point(211, 491);
            this.btnCobrar.Name = "btnCobrar";
            this.btnCobrar.Size = new System.Drawing.Size(89, 28);
            this.btnCobrar.TabIndex = 17;
            this.btnCobrar.Text = "COBRAR";
            this.btnCobrar.UseVisualStyleBackColor = true;
            this.btnCobrar.Click += new System.EventHandler(this.btnCobrar_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Tragamonedas.Properties.Resources.limon;
            this.pictureBox2.Location = new System.Drawing.Point(154, 115);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(105, 101);
            this.pictureBox2.TabIndex = 18;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Tragamonedas.Properties.Resources.limon;
            this.pictureBox3.Location = new System.Drawing.Point(278, 115);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(105, 101);
            this.pictureBox3.TabIndex = 19;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Tragamonedas.Properties.Resources.naranja;
            this.pictureBox4.Location = new System.Drawing.Point(278, 243);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(105, 101);
            this.pictureBox4.TabIndex = 22;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Tragamonedas.Properties.Resources.naranja;
            this.pictureBox5.Location = new System.Drawing.Point(154, 243);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(105, 101);
            this.pictureBox5.TabIndex = 21;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Tragamonedas.Properties.Resources.cerezas;
            this.pictureBox6.Location = new System.Drawing.Point(26, 243);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(105, 101);
            this.pictureBox6.TabIndex = 20;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Tragamonedas.Properties.Resources.cerezas;
            this.pictureBox7.Location = new System.Drawing.Point(278, 366);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(105, 101);
            this.pictureBox7.TabIndex = 25;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::Tragamonedas.Properties.Resources.campana;
            this.pictureBox8.Location = new System.Drawing.Point(154, 366);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(105, 101);
            this.pictureBox8.TabIndex = 24;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::Tragamonedas.Properties.Resources.naranja;
            this.pictureBox9.Location = new System.Drawing.Point(26, 366);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(105, 101);
            this.pictureBox9.TabIndex = 23;
            this.pictureBox9.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 531);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnCobrar);
            this.Controls.Add(this.btnjugar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtcredito);
            this.Controls.Add(this.txtjugado);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label);
            this.Controls.Add(this.btn2euro);
            this.Controls.Add(this.btn1euro);
            this.Controls.Add(this.btn50);
            this.Name = "Form1";
            this.Text = "TRAGAMONEDAS EuroDAM";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn50;
        private System.Windows.Forms.Button btn1euro;
        private System.Windows.Forms.Button btn2euro;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtjugado;
        private System.Windows.Forms.TextBox txtcredito;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnjugar;
        private System.Windows.Forms.Button btnCobrar;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
    }
}

