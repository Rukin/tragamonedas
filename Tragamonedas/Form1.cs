﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tragamonedas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        double credito;

        double jugado;

        private void ActualizarCredito()
        {
            txtcredito.Text = credito.ToString();
        }

        private void ActualizarJugado()
        {
            txtjugado.Text = jugado.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btn1euro_Click(object sender, EventArgs e)
        {
            credito += 1;
            ActualizarCredito();
        }

        private void btn2euro_Click(object sender, EventArgs e)
        {
            credito += 2;
            ActualizarCredito();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btn50_Click(object sender, EventArgs e)
        {
            credito += 0.5;
            ActualizarCredito();
        }

        private void txtcredito_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtjugado_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label_Click(object sender, EventArgs e)
        {

        }

        private void btnjugar_Click(object sender, EventArgs e)
        {
            if (credito >= 0.5)
            {
                credito -= 0.5;
                jugado += 0.5;
                ActualizarCredito();
                ActualizarJugado();
            }
            else
            {
                MessageBox.Show("No tienes suficiente dinero");
                return;
            }
        }

        private void btnCobrar_Click(object sender, EventArgs e)
        {
            if (credito == 0 && jugado == 0)
            {
                MessageBox.Show("No has jugado ni metido dinero todavía");
                return;
            }
            else
            {
                MessageBox.Show("Has ganado " + credito + " €" +
                    "\nY te has gastado " + jugado + " €");
                credito -= credito;
                jugado -= jugado;
                ActualizarCredito();
                ActualizarJugado();
            }
        }
    }
    }
